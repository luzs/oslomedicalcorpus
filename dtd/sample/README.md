# Draft DTDs for Oslo Medical Corpus (OMC) and sample text 

This directory contains:

- omcheader.dtd: the OMC header file specification, in DTD format

- omctext.dtd: the OMC text file specification, in DTD format

- EMBC-paper-sample.pdf: a PDF file containing a sample text to be
  encoded/ (NB: This a draft of one of our papers, so please don't
  redistribute.)

- omcsample.xml: a sample text encoded according to the DTD (I omitted
  parts of the paper, as this only an example.)

- several PNG files, containing the visuals extracted from the sample
  PDF file (tables and figures)
  
- omcsample.hed: the corresponding header file.

## Annotation of visuals

I am proposing annotating visuals according to the taxonomy proposed
by Desnoyers (2011). This taxonomy is structured as follows:

1. cosmograms
   1. topograms
   2. reigrams
2. typograms
   1. scriptograms
   2. organigrams
   3. cellulograms
3. analograms
   1. punctigrams
   2. curvigrams
   3. puncticurvigrams
   4. histograms
   5. morphograms

The first level represent classes, and the sencond level represent
orders. There are also two further levels for families and qualifiers,
which further specify the visuals. These are endoced in the DTD as XML
hierarchies. It would probably be a good idea for the annotators to
have the paper next to them for reference until they get used to the
terminology.


## Refs

- Desnoyers, Luc (2011). 'Toward a Taxonomy of Visuals in Science
  Communication', Technical Communication , Vol. 58, No. 2 (May 2011),
  pp. 119-134.
  <https://www.jstor.org/stable/10.2307/26464332>
