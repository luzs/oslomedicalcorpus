# How to use the modnlp/OMC text search box

The general syntax for OMC search is the following:

`word_1{+{[no_of_intervening_words]}word_2...}`

where curly brackets denote optional text, and the square brackets are
actual typed text (though optional). So, the following are examples of
valid queries:

`epidemiological`

`epidemiological+evidence`

`epidemiological+[4]evidence`

where the first would return (as of today) 293 lines, the second would
return 25 lines, and the third would return 26 lines. The extra line
returned by the third query includes a sentence where epidemiological
is not the only kind of evidence:

"A series of updated country Epidemiological Fact Sheets was published
that, for the first time, featured time trends of **epidemiological dynamics and scientiﬁc evidence** that indicates that providing antiretroviral therapy to this population..."

Searching for words to the left follow the same syntax:

`studies+[5]epidemiological`

returns 5 lines where the word "studies" appears up to 5 positions to
the left of "epidemiological".

The following options are currently supported by the keyword search
box, as possible values of `word_1` etc in the above described query
syntax:

- Single keywords: just type a single word into the keyword box.

    - Example: if you type `pandemic`, for instance, the tool will
  retrieve all occurrences of this word in the whole corpus (unless
  you have selected a sub-corpus). The search is case-insensitive by
  default (i.e. it will retrieve "pandemic", "Pandemic", "PANDEMIC"
  etc). You can specify case-sensitive search by ticking the
  appropriate box in the **Options** menu.

- *Wildcards* (a prefix followed by the character `*`): this allows
  you to select prefixes.

    - Example: typing in `epidem*` will retrive all words wich start
      with `epidem`, such as "epidemic", "epidemiological",
      "epidemiology" etc. 

- Regular expressions: this allows you to select any
  string that matches a regular expression (i.e. an element of a
  regular language). Regular expressions need to be enclosed in double
  quotation symbols (e.g. `"epidem.*"`) 

    - Examples:
    
      - typing in `".*demi.*"` will retrive all consisting of sequence
        of any charaters (represented by '.*') including an empty
        sequence, followed by the string "demi", followed by another
        sequence of characters. So this query would retrieve
        "epidemic", "pandemic", "epidemiology" etc. However, not that
        it would also retrieve "demise" (an empty character sequence,
        followed by "demi", followed by a character sequence, "se"),
        "Academia" etc.
        
    -  entering "(pan|epi)demi.*" would retrieve words consisting of
       "pan" or "epi",  followed by the string "demi", followed by any
        sequence of characters. Thie would retrieve "epidemic",
       "pandemic", "pandemics" etc.

  Regular expressions are too powerful to enumerate here, but if you
  are not familiar with them you can get reasonable tutorials and
  learning resources on the web. See, for instance: https://regexone.com/
  and https://regex101.com/

- Sequences: creating sequences by connecting a search token (single
  words, wildcards or regular expressions) with `+` allows you to
  specify sequences of key words, and/or wildcards, and the maximum
  number of intervening words you wish to allow between each element
  in the sequence.

    - Examples:
      - Entering `seen+before` will find "...never seen before..." etc; 
      - entering `seen+[1]before` finds, in addition, "...seen her
        before...", "...seen this before...", and all sequences in
        which there is at most one word between "seen" and "before".

 Combinations of words and wildcards are also allowed, so entering
 `know+before*` will find "...know before...", "...know beforehand",
 etc. 

 Sequences can be a way to limit retrieval of large numbers of items.
 For the above described query `"(pan|epi)demi.*"`, for instance, the
 system would return 4,436 concordances. Say, however, that you are
 interested in lines that have something to do with control of
 pandemics/epidemics. You might then enter, say,
 `control+[6]"(pan|epi)demi.*"` which would limit the concordance to
 53 lines, where the word "control" appears up to 6 positions to the
 left of "pandemic", or "epidemic", or "epidemics", or
 "epidemiological" etc.
 
