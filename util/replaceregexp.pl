#!/usr/bin/perl
#
use File::Copy;
use Getopt::Std;

sub Usage {
die "Usage: summarisevisuals.pl  files
           Extract visual descriptors from file.xml, flatten them and 
           add the flattened version to a CSV file.
           Options:
             -h        display this message
             -v        verbose
             -c        print CSV header
             -f file   file from which to extract visuals summary
\n"; 
}
getopts('hvc');

Usage()
    if ($opt_h );
    
$textfile = $opt_f;

$debug = $opt_v;

## print CSV header
print "file,image,class,order,family,qualifier,type,axis,line,orientation,arrangement\n"
    if $opt_c;
 

foreach ( @ARGV ){
    ExtractVisuals($_);
}

sub ExtractVisuals{
    my $t = shift;

    my $err = 0;
    
    print STDERR "Processing $t\n";
    ## get txt source
    my $txt = '';
    open(TF, "$t") or die "Couldn't open $t: $!\n";
    while (<TF>){
        $txt .= $_;
    }
    close TF;               

    
    ## return CSV lines
    while ($txt =~ /<visual>(.*?)<\/visual>/sg){
        my $v = $1;
        $t =~ s!(.*/|)(.+)!\2!;
        my $tx = "$t,".GetImageFilename($v).",";
        $tx .=  GetVisualDescriptor($v)."\n";
        $ct++;
        print $tx;
    }

    return $err;
}


sub GetVisualDescriptor {
    my $vis = shift;

    if ($vis =~ /.*?<class>(.*?)<\/class>/s){
        my $t = $1;
        if ($t =~ /.*?<(.*?)>.*?<(.*?)>.*?<([a-zA-Z]+?)( +.*?=.*?|)>.*?<([^ ]+?)( +.*?=.*?|)>.*?(<description>.*?<\/description>|).*/s ){
            my $r = "$1,$2,$3,$5,";
            $attstr = "$6";
            ## print "$attstr\n";
            %atts = ('type', 'NA',
                     'axis', 'NA',
                     'line', 'NA',
                     'orientation', 'NA',
                     'arrangement', 'NA');
            while ($attstr =~ /(type|axis|line|orientation|arrangement)=["'](.+?)["']/sg){
                $atts{$1} = $2;
            }
            return $r."$atts{'type'},$atts{'axis'},$atts{'line'},$atts{'orientation'},$atts{'arrangement'}";
        }
        else {
            return "<!-- Taxon parsing failed -->\n";
            warn("Taxon parsing failed $!");
        }
    }
    else {
        return "<!-- Class parsing failed -->\n";
        warn("Class parsing failed $!");
    }
}

sub GetImageFilename {
    my $vis = shift;

    if ($vis =~ /.*?<image\s+uri=['"](.+?)['"].*?>/s){
        return "$1";
    }
}

sub GetImageCaption {
    my $vis = shift;
    
    if ( $vis =~ /.*?(<caption>.*?<\/caption>)/s ){
        my $cap = $1;
        $cap =~ s/\n/ /sg; 
        return $cap;
    }
}

