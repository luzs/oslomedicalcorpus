## Some utilities to facilitate annotation

- [visualstoheaders.pl](visualstoheaders.pl): extract visuals from text
  files (.xml), convert them to 'flat' versions suitable for
  sub-corpus indexing, and add them to header (.hed) files.

  Usage:
  
  ```sh
  perl ../util/visualstoheaders.pl -f omc000003.hed
  # Generates  omc000003.hed (new version) and saves old as omc000003.hed-bkp
  
  ## if you wish to convert all hed files, do:
  
  for f in *.hed ; do echo "Processing $f" ; perl  ../util/visualstoheaders.pl -f $f ; done
  ```
  
- Removing an XML tag and it's contents

```sh
sed -i 's/<url>.*\?<\/url>/<url\/>/' omc*.xml 
```

  or better
  
```sh
perl -i~ -0777 -pe 's/<url>[^<]*?<\/url>//g' omc*.xml
  for f in `grep -l http *.xml` ; do perl -i~ -0777 -pe 's/https?:\/\/[^), ]*?[), ]/<URL\/>/g' $f  ; done 
```

  -i~ will edit the file "in place", leaving a backup copy
  -0777 reads the whole file at once, not line by line

  summarise changes (for checking):
  
  
- A one-line unix shell command to convert author tags

```sh
for f in `ls *.hed` ; do echo "converting $f"; cat $f |perl -e '$t = ""; while(<>){$t .= $_;} $t =~ s/<firstname>(.*?)<\/firstname>.*?<lastname>(.*?)<\/lastname>/if ("$1" and "$2") {"$2, $1"} else {"$1 $2"}/sge; print $t;' | sponge $f ; done
```
- copying too many files (when cp * /dest/ fails with 'too many
  arguments')

```sh
  find /disk2/omc/headers/ -maxdepth 1 -name '*.png' -exec sudo cp {} . \;
```

- Removing pseudo-organisations from headers:

```sh
for f in *.hed ; do mv $f $f-old ; sed 's/<organisation[^>]\+name *= *.The_Conversation.[^>]*>//' $f-old > $f ; done
for f in *.hed ; do mv $f $f-old ; sed 's/<organisation[^>]\+name *= *.Jason_Hickel_Blog.[^>]*>//' $f-old > $f ; done
for f in *.hed ; do mv $f $f-old ; sed 's/<organisation[^>]\+name *= *.Democracy_Now.[^>]*>//' $f-old > $f ; done
```
