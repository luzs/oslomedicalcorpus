#!/bin/sh
# release-webcli.sh
# Created Fri Sep 15 2023 by luzs s.luz@ed.ac.uk
# $Id$
# $Log$

cd /var/www/html
sudo rm -rf webcli-old/
sudo mv webcli webcli-old
sudo cp -a /disk2/software/modnlp-code/webcli/ .
