#!/usr/bin/perl
#
use File::Copy;
use Getopt::Std;
use utf8;

sub Usage {
die "Usage: midsentencevisuals.pl [-h|-a|-v|-f] 
           Move visuals from mid-sentences to the beginning of previous sentence,
           and reformat 'image' tag so that contents of 'caption' and 
           'description' are set as attributes of 'image', unless -a is 
           specified.     
           
           add the flattened version to the righ sections in file.hed.
           Options:
             -h        display this message
             -a        do not reformat image arguments
             -v        verbose
             -f file   the file to receive the flat visual descriptors
\n"; 
}
getopts('havf:');

Usage()
    if ($opt_h || !$opt_f);
    

$debug = $opt_v;

$textfile = $opt_f;

print "Moving visual tags to end of paragraph in $textfile".
    (!$opt_b ? "; saving original as $textfile-bkp." : "")."\n"
    if $debug;

moveVisualTags($textfile);

sub moveVisualTags{
    my $t = shift;

    ## get txt source
    open(TF, "$t") or die "Couldn't open $t: $!\n";
    while (<TF>){
        $txt .= $_;
    }
    close TF;

    my $ct = 0;
    my $ctvis = 0;
    ## build flat visuals for hed
    $txt =~ s/([.?!:>]\)?"?”?(?![a-zA-Z0-9]| *\n<visual))([^.?!:>]+?)(\s*)(<visual[^>]*>.*?<\/visual>?)(\n?)/$ct++; print STDERR "_$1|" if $opt_v; "$1$3$4\n$2 ";/gse;

    if (!$opt_a) {
        my @elements = ('caption', 'description');
    
        foreach my $element ( @elements ) {
            $txt =~ s/<visual>(.*?)<\/visual>/ReformatVisual($1, $element, \$ctvis)/gse;
            #$txt =~ s/(<visual>.*?)<image([^>]*?)\/>(.*?) *<$element>(.*?)<\/$element>\n?(.*?<\/visual>)/print STDERR "Matched $element='$4'; reshaped as\n".ReshapeImage($2,$element, $4)."\n" if $opt_v; $ctvis++; "$1".ReshapeImage($2,$element, $4)."$3$5"/gse;
        }
        
    }

    print $txt;
    print STDERR ($ct && $debug)?
        "\n=====Moved $ct visuals in $t\n=====Made $ctvis changes to visuals\n"
        : "=====Moved $ct visuals in $t\n=====Made $ctvis changes to visuals\n"
}

sub ReformatVisual {
    my $vis = shift;
    my $element = shift;
    my $ctref = shift;
    
    $vis =~ s/(.*?)<image([^>]*?)\/>(.*?) *<$element>(.*?)<\/$element>\n?(.*?)/print STDERR "Matched $element='$4'\n" if $opt_v; $$ctref++; "$1".ReshapeImage($2,$element, $4)."$3$5"/gse;

    return "<visual>$vis</visual>";
}


sub ReshapeImage {
    my $args = shift;
    my $newname = shift;
    my $newvalue = shift;

    $newvalue =~ s/"/'/gs;
    $newvalue =~ s/\n/ /gs;
    $newvalue =~ s/>/&gt;/gs;
    $newvalue =~ s/</&lt;/gs;
    $newvalue =~ s/^\s+|\s+$//gs;
    my $newarg = $newvalue? " $newname=\"$newvalue\"" : '';
    my $out = "<image$args$newarg/>";
    return $out;
}
    
