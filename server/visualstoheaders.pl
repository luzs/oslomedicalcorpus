#!/usr/bin/perl
#
use File::Copy;
use Getopt::Std;
use utf8;

sub Usage {
die "Usage: visualtoheaders.pl [-h|-b|-v|-w] -f file.hed
           Extract visual descriptors from file.xml, flatten them and 
           add the flattened version to the righ sections in file.hed.
           Options:
             -h        display this message
             -b        do not create backup file (use with caution!)
             -v        verbose
             -w        rewrite backup files (use with caution!)
             -f file   the file to receive the flat visual descriptors
\n"; 
}
getopts('hbvwf:');

Usage()
    if ($opt_h || !$opt_f);
    
$headfile = $opt_f;
$textfile = $headfile;

$debug = $opt_v;

$textfile =~ s/\.hed//i;
$textfile .= '.xml';

print "Copying section tags from $textfile to $headfile".
    (!$opt_b ? "; saving original as $headfile-bkp." : "")."\n"
    if $debug;

if (!$opt_b && !$opt_w && -e "$headfile-bkp") {
    die("Error: file $headfile-bkp exists. Move or copy it before proceeding.");
}
copy($headfile, "$headfile-bkp")
    if !$opt_b;

copySectionTags($textfile, $headfile);

sub copySectionTags{
    my $t = shift;
    my $h = shift;

    my $indexelement = 'section';
    my $indexattribute = 'id';

    %sectionvisuals = ();
    %sectioncts = ();    
    my $err = 0;
    
    ## get txt source
    open(TF, "$t") or die "Couldn't open $t: $!\n";
    while (<TF>){
        $txt .= $_;
    }
    close TF;

    ## build flat visuals for hed
    while ($txt =~ m/<($indexelement).+?id=['"](.*?)['"].*?>(.*?)<\/$indexelement>/gs){
        my $id = $2;
        my $sc = $3;
        my $tx = '';
        my $ct = 0;
        my $ps = $-[3];
        while ($sc =~ m/<visual>(.*?)<\/visual>/sg){
            my $v = $1;
            my $p = $ps + $-[0];
            my $caption = GetImageCaption($v);
            $tx .= "      <visual pos='$p'>\n";
            $tx .=  '        '.GetImageTag($v)."\n";
            $tx .=  '        '.$caption."\n" if $caption;
            $tx .=  '        '.FlattenVisualTree($v)."\n";
            $tx .= "      </visual>\n";
            $ct++;
        }
        $tx =~ s/\n$//;
        #print "ct: $ct\n";
        $sectioncts{$id} = $ct
            if ($tx);
        $sectionvisuals{$id} = $tx 
            if ($tx);
    }

    ## write new hed
    open(HF, "$h") or die "Couldn't open $h: $!\n";
    $txt = '';
    ## replace sections from header
    while (<HF>){
        $txt .= $_;
    }
    ## remove old visuals (which will be replaced by updated versions) 
    $txt =~  s/\s*<visual( |>).*?>.*?<\/visual>\s*\n?//sg;

    close HF;

    $txt =~ s/<$indexelement +id=['"](.*?)['"](.*?)(\/?)>/$ct = $sectioncts{$1} || 0;print("Adding $ct taxon elements to section $1\n");$secend = $3? "    <\/section>" : '';$nl = $ct? "\n":'';"<$indexelement id='$1'$2>$nl$sectionvisuals{$1}$nl".$secend/gse;
    
    open(HT, ">$h") or die "Couldn't open $h: $!\n";
    print HT $txt;
    close HT;

    return $err;
}


sub FlattenVisualTree {
    my $vis = shift;

    my $desc = '';
    if ($vis =~ /.*?<image.*?(description=['"].*?['"])[^>]*>.*/s){
        $desc = GetAttributeValue($1);
        $desc = "<description>$desc</description>" if $desc;
    }
    if ($vis =~ /.*<class>(.*?)<\/class>/s){
        my $t = $1;
        if ($t =~ /.*?<(.*?)>.*?<(.*?)>.*?<([a-zA-Z]+?)( +.*?=.*?|)>.*?<([^ >]+?|)( +[^>]*?=[^>]*?|)>.*/s ){
            my $r = "<taxon class='$1' order='$2' family='$3' $4";
            my $s = "$5";
            $s = $s =~ m|/.*| ? "" : " qualifier='$s' $6";
            my $r = "$r$s>$desc</taxon>";
            $r =~ s/ >/>/;
            $r =~ s/'/"/g;
            return $r;
        }
        else {
            return "<!-- Taxon parsing failed -->\n";
        }
    }
    else {
        return "<!-- Class parsing failed -->\n";
    }
}

sub FlattenVisualTreeOld {
    my $vis = shift;

    if ($vis =~ /.*?<class>(.*?)<\/class>/s){
        my $t = $1;
        if ($t =~ /.*?<(.*?)>.*?<(.*?)>.*?<([a-zA-Z]+?)( +.*?=.*?|)>.*?<([^ ]+?)( +.*?=.*?|)>.*?(<description>.*?<\/description>|).*/s ){
            my $r = "<taxon class='$1' order='$2' family='$3' $4 qualifier='$5' $6>$7</taxon>";
            $r =~ s/ >/>/;
            $r =~ s/'/"/g;
            return $r;
        }
        else {
            return "<!-- Taxon parsing failed -->\n";
        }
    }
    else {
        return "<!-- Class parsing failed -->\n";
    }
}

sub GetImageTag {
    my $vis = shift;

    if ($vis =~ /.*?<image\s+uri=(['"].+?['"]).*?>/s){
        return "<img src=$1/>";
    }
    else{
        warn "ERROR: could not extract image URI from $vis\n";
    }
}

sub GetImageCaption {
    my $vis = shift;
    
    if ( $vis =~ /.*?<image.*?caption=['"](.*?)['"]/s ){
        my $cap = $1;
        $cap =~ s/\n/ /sg; 
        return "<caption>$cap</caption>";
    }
}

sub GetImageCaptionOld {
    my $vis = shift;
    
    if ( $vis =~ /.*?(<caption>.*?<\/caption>)/s ){
        my $cap = $1;
        $cap =~ s/\n/ /sg; 
        return $cap;
    }
}

sub GetAttributeValue {
    my $av = shift;
    if ($av =~ /\w+=['"](.*?)['"]/s){
        return $1;
    }
    else {
        return '';
    }
}   
