#!/usr/bin/perl
# 
# EXPANDLIGATURES.PL --- Expand some UTF8 ligatures

# Author:  <s.luz@ed.ac.uk>
# Created: 05 Jul 2023
# $Revision$
# Keywords:


#     Permission  to  use,  copy, and distribute is hereby granted,
#     providing that the above copyright notice and this permission
#     appear in all copies and in supporting documentation.

# Commentary:

# Change log:
#

# Code:
#use warnings;
#use utf8;
use Getopt::Std;

my $version = '0.1';

sub usage {
    die<<"END";

  $me version $version


  usage: $me  [-h|-d|-n|-v] -f filename  

  Options:
      -f \t fname \t\t Input file
      -d \t dry-run (don't rewrite files, just show what would be done)
      -n \t do NOT create backup copy 
      -h \t Help   \t just display this message and quit.
      -v \t Verbose output
END
}

getopts('hdf:n');
usage
    if ( $opt_h || !$opt_f );



## incomplete list of most common ligatures 
my %ligatures = (
    'ﬀ' => 'ff',
    'ﬁ' => 'fi',
    'ﬂ' => 'fl',
    'ﬃ' => 'ffi',
    'ﬄ' => 'ffl',
    'ﬅ' => 'ft',
    'ﬆ' => 'st',
);
 
my $infname = "$opt_f.preexpligature";
if ($opt_d){
    $infname = "$opt_f";
}
else {
    rename($opt_f, $infname);
}
my $content = '';

open(FI, $infname) or die ("error opening $infname: $!");
while(<FI>){
  $content .= $_;
}
close FI;

my $expcontent = expandLigatures($content); 

exit
    if ($opt_d);


open(FO, ">$opt_f") or die ("error opening $opt_f: $!");

print FO $expcontent;

close FO;

unlink($infname)
    if $opt_n;

sub expandLigatures {
    my ($text) = @_;
 
    if (@_ != 1) {
        warn 'expandLigatures argument missing';
        return;
    }

    print STDERR "Processing $opt_f\n";
    for my $lig (keys %ligatures) {
        print STDERR "  Searching for $lig\n"
            if $opt_v;
        $text =~ s/$lig/{print "    Replaced $lig in $opt_f\n"; $ligatures{$lig};}/ge;
        #$text =~ s/$lig/$ligatures{$lig}/g;
    }
 
    return $text;
}
