# Server setup instructions

Recommended layout for the data/index directory:

```sh
omc/
├── dtd
├── headers
├── incoming
├── index
├── log
├── software
│   ├── modnlp-idx
│   └── modnlp-tecser
└── text
```

where modnlp-tecser and modnlp-idx will contain the latest binary
distribution of these packages, [downloadable from sourceforge
(-bin-gok.tgz files)](https://sourceforge.net/projects/modnlp/files/)

I suggest placing the DTDs in `dtd/` and symlinking `omcheader.dtd`
and `omctext.dtd` to `headers/` and `text/`, respectively. I would
also suggest copying all files in this folder to `incoming/`.  This is
the directory where you can put your `.xml`, `.hed` and image files
for indexing.

The program `indexincoming.pl` does several kinds of sanity checks,
translate ligatures into sensible letter, extracts the visual tags
from the text files into the header files, and several other things.
Here's it's help text:

```sh
 ./indexincoming.pl -h
Usage: indexincoming.pl [-h|-d|-l|-s|-q|-o|-x|-v]
             -h        display this message
             -i        skip image copying (assume they are already at destination 'headers/')
             -v        extract visual tags from xml, convert them to flat-format and add to hed files
             -l        expand ligatures (some at least)
             -d        dry run (test but do not index)
             -s        do not try to kill server before indexing
             -q        do not display debug messages
             -o        print all STDERR messages to STDOUT
             -x        skip 'file already indexed' test
  Please set locations correctly in config.pl.
```

As the last line says, you need to set up you config.pl file according
to your servers/data repository settings. Here is an exmaple:

```sh
## set these variables to point to your corpus files
$INDEX_DIR = '/data/omc//index';
$TEXT_DIR = '/data/omc//text';
$HEADERS_DIR = '/data/omc//headers';
$HEADERS_URL = 'http://some-website.org/headers/';
$IDX_BIN='/data/omc/software/modnlp-idx/';

$TEC_BIN="/data/omc/software/modnlp-tecser";
$TEC_SERVER="/usr/bin/java";
$ARGS="-Xms400m -Xmx450m -jar tecser.jar";
$SERVER_PID="/var/run/tecser/omc.pid";
$SYSLOGFILE="/var/log/tec/tecsys.log";
$ERRLOG="/var/log/tec/tecsys-error.log";

$DOS2UNIX='dos2unix';
# uncomment the following if you don't have d2u installed
#$DOS2UNIX='';
$XMLLINT='xmllint -noout -valid';
# uncomment the following if you don't have xmllint installed
#$XMLLINT='';
# comment out the following if you don't have file installed
$FILETYPE='file -bi';
$VISUALTOHEAD='visualstoheaders.pl';
$EXPANDLIGATURES="$pgd/expandligatures.pl -n -f";
$VISUALTOHEAD="$pgd/visualstoheaders.pl -v -b -f";

```

Look also at the documentation (README files, etc) in the `modnlp-idx`
and `modnlp-tecser` packages for furter details on how to index files
and set up a server.

## Software required

As may have gathered from the config file above, running these sanity
checks will require a number of other programs. The above setup was
designed for a Unix environment and tested on Linux (several versions
of Debian, Ubuntu, and CentOS). The software you must have installed
comprises:

- web server (the OMC runs on [Apache](https://httpd.apache.org/))

- A Java interpreter, JDK 8 or greater (we have used
[OpenJDK](https://openjdk.org/)

- The [modnlp indexer and server](https://sourceforge.net/projects/modnlp/files/):
  o modnlp-idx-x.y.z-bin-gok.tar.gz 
  o modnlp-tecser-a.b.c-bin-gok.tar.gz
    (where x.y.z and a.b.c denote the latest releases) and 

- [`dos2unix`](https://dos2unix.sourceforge.io/), a command line utility
  that converts MS-DOS format files into Unix format
  
- `xmllint`, an XML parser, validator

- `file`, a command line utility to determine the file type


