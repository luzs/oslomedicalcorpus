# Oslo Medical Corpus: DTDs, XSDs etc

This repository contains the XML definitions for the OMC and other
resources to be added.

See directory dtd and dtd/sample to get started. The
[dtd/sample/omcsample.xml](dtd/sample/omcsample.xml) 
file contains an example, showing how
[dtd/sample/EMBC-paper-sample.pdf](dtd/sample/EMBC-paper-sample.pdf) was annotated.

For annotation of visuals according to Desnoyer's taxonomy, see
[visualization/desnoyers-taxonomy-cheatsheet.pdf](visualization/desnoyers-taxonomy-cheatsheet.pdf). 
The source for this
PDF is [visualization/desnoyers-taxonomy-cheatsheet.svgz](visualization/desnoyers-taxonomy-cheatsheet.svgz), which is
editable with [Inkscape](http://inkscape.org).




A webpage containing links to the project resources can be accessed through this link: [https://luzs.gitlab.io/oslomedicalcorpus/](https://luzs.gitlab.io/oslomedicalcorpus/).

