# -*- coding: utf-8 -*-
import sys
import os
import re
from pathlib import Path
import shutil
import glob

descriptors_re = r'"(cosmogram|typogram|analogram|topogram|reigram|bothorders|ecogram|domogram|descriptive|statistical|reigramfamily|descriptive|functional|cinematic|transformational|bothordersfamily|photographic|pictographic|scriptogram|cellulogram|organigram|scriptogramfamily|phrase|equation|list|cellulogramfamily|content|format|organigramfamily|arborescent|intersecting|content|structure|punctigram|curvigram|puncticurvigram|histogram|histocurvigram|morphogram|punctigramfamily|univariate|bivariate|trivariate|curvigramfamily|puncticurvigramfamily|puncticurvigramqualifier|absolute|proportional|band|circle|propband|histocurvigramfamily|histocurvigramqualifier|radial|polygonal|chernoff)'

def main(idir, odir):
    with open(odir+'/filelist.txt') as f:
        files = f.read().splitlines()        
        i = 0
        in_visual = False 
        visual_re = r'</?visual>'
        visual_class_re = re.compile(r'<visual>(.*?)<class>(.*?)</class>(.*?)</visual>', re.DOTALL)

        for file in files:
            print("Processing "+file)
            xml = open(idir+'/text/'+file+'.xml', encoding='utf-8')
            p = Path(odir+'/'+file)
            p.mkdir()
            text = xml.read()
            text = re.sub(visual_class_re, r'<visual>\1<class>\n    [ENTER THE CLASSIFICATION HERE]\n  </class>\3</visual>', text)
            out = open(odir+'/'+file+'/'+file+'.xml', mode='w', encoding='utf-8')
            print(text, file=out)
            for f in glob.glob(idir+'/headers/'+file+"-*.png"):
                shutil.copy(f, odir+'/'+file+'/')
            for f in glob.glob(idir+'/headers/'+file+"-*.jpg"):
                shutil.copy(f, odir+'/'+file+'/')
            for f in glob.glob(idir+'/headers/'+file+"-*.gif"):
                shutil.copy(f, odir+'/'+file+'/')
            out.close()
            xml.close()


if __name__ == "__main__":
   main(sys.argv[1],sys.argv[2])




