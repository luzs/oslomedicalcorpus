{::options parse_block_html="true" /}
# Annotation disagreements for visual at the class level (9 in total)
### Visual: omc000001-figure-6.png, file: omc000001.xml
 ![omc000001-figure-6.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000001-figure-6.png)

 .      . |          :thumbsup: **` Annotator 1 `** |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |            analogram |             typogram
  Family  |     punctigramfamily |     organigramfamily
  Order   |           punctigram |           organigram

**Comment**: This could be considered a punctigram as it represents points on a
 timeline. While most punctigrams are represented in 2D, 1D
 representations such as the above should also be acceptable. 

### Visual: omc000021-figure-1.png, file: omc000021.xml
 ![omc000021-figure-1.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000021-figure-1.png)

 .      . |          Annotator 1 |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |            cosmogram |             typogram
  Family  |        reigramfamily |    scriptogramfamily
  Order   |              reigram |          scriptogram
  
**Comment**: visual removed in subsequent update. 
  
 ### Visual: omc000026-fig-11.4.png, file: omc000026.xml
 ![omc000026-fig-11.4.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000026-fig-11.4.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |             typogram |            analogram
  Family  |    scriptogramfamily |         proportional
  Order   |          scriptogram |            histogram
  
**Comment**: This is clearly an analogram (a variation on the piechart
design). All such visuals annotated as scriptograms should be amended
accordingly. 

 ### Visual: omc000026-fig-6.8.png, file: omc000026.xml
 ![omc000026-fig-6.8.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000026-fig-6.8.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |             typogram |            analogram
  Family  |    scriptogramfamily |         proportional
  Order   |          scriptogram |            histogram
  
**Comment**: This is clearly an analogram (a variation on the piechart
design). All such visuals annotated as scriptograms should be amended
accordingly. 

 ### Visual: omc000035-image-9.png, file: omc000035.xml
 ![omc000035-image-9.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000035-image-9.png)

 .      . |          :thumbsup: **` Annotator 1 `** |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |            cosmogram |             typogram
  Family  |        reigramfamily |    scriptogramfamily
  Order   |              reigram |          scriptogram

**Comment**: Either annotation would do, but I think Annotator 1's is
 a more accurate description. The text works more as a 'caption' than
 as a visual.

### Visual: omc000041-figure-3.png, file: omc000041.xml
 ![omc000041-figure-3.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000041-figure-3.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |             typogram
  Family  |             absolute |    cellulogramfamily
  Order   |            histogram |          cellulogram

**Comment**: Although visually similar to a histogram, this visual is
actually a stylised table, with colours representing categories
filling out each cell of the table. Therefore, Annotator 2 has it
right. Histograms represent *quantities* assigned to nonquantitative
entities. No quantity element is present in this visual. 

### Visual: omc000055-figure-12.png, file: omc000055.xml 
 ![omc000055-figure-12.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000055-figure-12.png)

 .      . |          Annotator 1 |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |            cosmogram |             typogram
  Family  |        reigramfamily |     organigramfamily
  Order   |              reigram |           organigram

**Comment**: This is a tricky one. It is a photograph (thus a reigram)
of a set of organigrams. I suppose its classification would depend on
the role the visual plays in the text (e.g. as a photo showing samples
of teamwork which produced diagrams about AMR, in which case it would
be a reigram, or as diagrams about AMR, which support some point made
in the text, in which case it should be annotated as an organigram.)

### Visual: omc000065-fig-8.png, file: omc000065.xml
 ![omc000065-fig-8.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000065-fig-8.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |             typogram
  Family  |         proportional |    cellulogramfamily
  Order   |            histogram |          cellulogram

**Comment**: As in the case above, though visually similar to a
histogram, this visual is actually a stylised table, with colours
representing categories filling out each cell of the table. Therefore,
Annotator 2 has it right.

### Visual: omc000070-pic-5.png, file: omc000070.xml
 ![omc000070-pic-5.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000070-pic-5.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            cosmogram |             typogram
  Family  |        reigramfamily |    scriptogramfamily
  Order   |              reigram |          scriptogram

**Comment**: In my opinion this visual would function as a scriptogram,
even if it were a collage of newspaper headlines.
