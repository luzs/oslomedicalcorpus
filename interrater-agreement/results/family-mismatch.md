# Annotation disagreements for visual at the family level (22 in total)
### Visual: omc000001-figure-14.png, file: omc000001.xml
 ![omc000001-figure-14.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000001-figure-14.png)

 .      . |          Annotator 1 |          :thumbsup: **`Annotator 2`**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  |     punctigramfamily |             absolute
  Order   |           punctigram |            histogram
 
 **Comment**: This should be rather classified as a histogram, since
 the 'points' are only used to associata a quantity (% patients
 reatined) to a category (country). The 'points' in fact could be
 interpreted as indicating the heights of bars in a standard
 histogram.
 
 
 ### Visual: omc000001-figure-29.png, file: omc000001.xml
 ![omc000001-figure-29.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000001-figure-29.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  |     punctigramfamily |             absolute
  Order   |           punctigram |            histogram
  
 **Comment**: This is the same as in the provious case, except that
 the 'bars' (denoted by the points relating a quantity to a category)
 are horizontal.
 
### Visual: omc000014-fig-16.png, file: omc000014.xml
 ![omc000014-fig-16.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000014-fig-16.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |             typogram |             typogram
  Family  |    scriptogramfamily |     organigramfamily
  Order   |          scriptogram |           organigram
  
 **Comment**: I agree with Annotator 2 in this case, as organigrams
 are diagrams that 'display components of a system and their
 relations'. 
 
### Visual: omc000014-table-01.png, file: omc000014.xml
 ![omc000014-table-01.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000014-table-01.png)

 .      . |          Annotator 1 |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |             typogram |             typogram
  Family  |    cellulogramfamily |    scriptogramfamily
  Order   |          cellulogram |          scriptogram
  
 **Comment**: Removed in subsequent updates.
 
### Visual: omc000015-image-11.png, file: omc000015.xml
 ![omc000015-image-11.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000015-image-11.png)

 .      . |          :thumbsup: **` Annotator 1 `** |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |            cosmogram |            cosmogram
  Family  |        reigramfamily |              ecogram
  Order   |              reigram |             topogram
  
 **Comment**:  I think the original annotation (1) is corrected.  "Reigrams are
figurations of material objects, either natural or artificial."
 
### Visual: omc000015-image-12.png, file: omc000015.xml
 ![omc000015-image-12.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000015-image-12.png)

 .      . |          :thumbsup: **` Annotator 1 `** |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |            cosmogram |            cosmogram
  Family  |        reigramfamily |              ecogram
  Order   |              reigram |             topogram
  
 **Comment**: as above
 
### Visual: omc000016-fig-06.png, file: omc000016.xml
 ![omc000016-fig-06.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000016-fig-06.png)

 .      . |          :thumbsup: **` Annotator 1 `** |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |             typogram |             typogram
  Family  |    scriptogramfamily |     organigramfamily
  Order   |          scriptogram |           organigram
  
 **Comment**: Typogram (though there are organigram elements in it).
 
### Visual: omc000023fig22.png, file: omc000023.xml
 ![omc000023fig22.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000023fig22.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  |     punctigramfamily |             absolute
  Order   |           punctigram |            histogram
  
 **Comment**: This is yet another histogram (for the reasons given in
 the examples above). The 'points' stand for the heights of bars
 relating **quantities** (ratio of consumer price to international
 reference) to a **categories** (sector/income level).
 
### Visual: omc000027-picture-16.png, file: omc000027.xml
 ![omc000027-picture-16.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000027-picture-16.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            cosmogram |            cosmogram
  Family  |        reigramfamily |             domogram
  Order   |              reigram |             topogram
  
 **Comment**: This is clearly a domogram ("Photography of man-made
 constructions"; Desnoyers' description).
 
### Visual: omc000031-figure-4-11.png, file: omc000031.xml
 ![omc000031-figure-4-11.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000031-figure-4-11.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  | histocurvigramfamily |             absolute
  Order   |       histocurvigram |            histogram
  
 **Comment**: As there is no curve on this graph, it should be
 classified as a plan histogram.
 
### Visual: omc000031-figure-4-12.png, file: omc000031.xml
 ![omc000031-figure-4-12.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000031-figure-4-12.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  | histocurvigramfamily |             absolute
  Order   |       histocurvigram |            histogram
  
 **Comment**: As above.
 
### Visual: omc000031-figure-7-1.png, file: omc000031.xml
 ![omc000031-figure-7-1.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000031-figure-7-1.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  |     punctigramfamily | puncticurvigramfamily
  Order   |           punctigram |      puncticurvigram
  
 **Comment**: I would probably label this a histogram, given that it
 primarily relates quantities to categories. However, it also has
 elements of a puncticurvigram, so I think Annotator 2 is closer to
 the truth than Annotator 1, in this case. 
 
### Visual: omc000031-figure-8-6.png, file: omc000031.xml
 ![omc000031-figure-8-6.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000031-figure-8-6.png)

 .      . |          :thumbsup: **` Annotator 1 `** |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  | histocurvigramfamily |             absolute
  Order   |       histocurvigram |            histogram
  
 **Comment**: I believe the original annotation is correct.
 
### Visual: omc000031-picture-13.png, file: omc000031.xml
 ![omc000031-picture-13.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000031-picture-13.png)

 .      . |          :thumbsup: **` Annotator 1 `** |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |            cosmogram |            cosmogram
  Family  |        reigramfamily |              ecogram
  Order   |              reigram |             topogram
  
 **Comment**: As above.
 
### Visual: omc000032-figure-46.png, file: omc000032.xml
 ![omc000032-figure-46.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000032-figure-46.png)

 .      . |          :thumbsup: **` Annotator 1 `** |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  | histocurvigramfamily | puncticurvigramfamily
  Order   |       histocurvigram |      puncticurvigram
  
 **Comment**: As above.
 
### Visual: omc000033-fig-19.png, file: omc000033.xml
 ![omc000033-fig-19.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000033-fig-19.png)

 .      . |          Annotator 1 |          Annotator 2
 -------- | -------------------- | --------------------
  Class   |            cosmogram |            cosmogram
  Family  |        reigramfamily |              ecogram
  Order   |              reigram |             topogram
  
 **Comment**: As above.
 
### Visual: omc000041-figure-27.png, file: omc000041.xml
 ![omc000041-figure-27.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000041-figure-27.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  |               radial |      curvigramfamily
  Order   |           morphogram |            curvigram
  
 **Comment**: This is a curvigram displayed on a radial axis. A
 morphograms displays a figure (such as a star or a polygon), which is
 not the case here. 
 
### Visual: omc000041-figure-6.png, file: omc000041.xml
 ![omc000041-figure-6.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000041-figure-6.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  |     punctigramfamily |             absolute
  Order   |           punctigram |            histogram
  
 **Comment**: This is yet another case of a (vertically orientated)
 histogram posing as a punctigram. This particular type of graph is
 known as a Cleveland Dot Plot. I think we should check all
 punctigrams in all files to see if there are further instances of
 this kind of mislabelling. We should also add a note on the
 difference between histograms and punctigrams to the annotator's
 manual.
 
### Visual: omc000041-figure-8.png, file: omc000041.xml
 ![omc000041-figure-8.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000041-figure-8.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  |     punctigramfamily |             absolute
  Order   |           punctigram |            histogram
  
 **Comment**: As above.
 
### Visual: omc000041-figure-9.png, file: omc000041.xml
 ![omc000041-figure-9.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000041-figure-9.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  |     punctigramfamily |             absolute
  Order   |           punctigram |            histogram
  
 **Comment**: As above.
 
### Visual: omc000042-fig-8-1.png, file: omc000042.xml
 ![omc000042-fig-8-1.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000042-fig-8-1.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            analogram |            analogram
  Family  |     punctigramfamily |             absolute
  Order   |           punctigram |            histogram
  
 **Comment**: As above.
 
### Visual: omc000042-pic-3.png, file: omc000042.xml
 ![omc000042-pic-3.png](https://genealogies.mvm.ed.ac.uk/omc/headers/omc000042-pic-3.png)

 .      . |          Annotator 1 |          :thumbsup: **` Annotator 2 `**
 -------- | -------------------- | --------------------
  Class   |            cosmogram |            cosmogram
  Family  |        reigramfamily |              ecogram
  Order   |              reigram |             topogram

 **Comment**: Removed.
