#!/usr/bin/perl
# 
# annotatate.cgi --- Test files for indexing

# Author: luzs <s.luz@ed.ac.uk>
# Created: 2 May 2022
# $Revision$
# Keywords:


#     Permission  to  use,  copy, and distribute is hereby granted,
#     providing that the above copyright notice and this permission
#     appear in all copies and in supporting documentation.

# Commentary:

# Change log:
#

# Code:
use strict;
use warnings;
use CGI;
use Cwd;
use List::Util qw/shuffle/;


my $q = CGI->new();
print $q->header;
print '<html><head><meta charset="utf-8"><title>OMC inter-rater reliability experiment</title></meta></head><body>';
print "<h2>OMC inter-rater reliability experiment</h2>";

my $samptable = "sampleira.csv";
my $annotable = "annotated.csv";
my $datadir   = "data";
my $baseurl   = "https://genealogies.mvm.ed.ac.uk/omc/headers";
my $xmlhead = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE omctext SYSTEM "omctext.dtd">
<omctext>
<document>
<section id="s1" type="text">
';
my $xmlfoot = '</section></document></omctext>';

open ST, "$samptable" || die "Error reading $samptable $!";

my @images = ();
my @annotators = ();
<ST>; ## discard header
while(my $r = <ST>){
    $r =~ s/^["'](.+)["']?$/$1/;
    my @cols = split(/","/,$r);
    push @annotators, lc($cols[1]);
    push @images, $cols[3];
}
close ST;

open AT, "$annotable" || die "Error reading $annotable $!";

## AT_col[0], AT_col[1]
## annotator_name, image_name.png
my %reimages = ();
while(my $r = <AT>){
    chomp $r;
    my @cols = split(/,/,$r);
    $reimages{$cols[1]} = lc($cols[0]);
    ## e.g. $reimages{'omc000011-fig.png'} = 'Jan'
}
close AT;

my @rand = shuffle 0..$#images; 
my $i = 0;
##print join(",", @rand)."<br>";
##print join(",", sort(@rand))."<br>";
my $annotator = $annotators[$rand[$i]];
my $image = $images[$rand[$i]];
my $ruser = lc($q->remote_user()) || '';
my $samplect = 0;
##print "<table><tr><td>IMAGE<td>RE-ANNOTATOR<td>SAMPLED ANNOTATOR<td>REMOTE USER<tr>\n";
##print "<td>$image <td>$reimages{$image} <td> $annotator<td> $ruser<tr>\n";

## look for an image not yet annotated by $ruser (remote user)
while  ($annotator eq $ruser  || exists($reimages{$image}) ){
    if ($i++ >= $#images){
        print "</table>";
        PrintOKMessage("No images left for you to annotate. Thanks");
        PrintCloseHtml();
        exit;
    }
    $annotator = $annotators[$rand[$i]];
    $image = $images[$rand[$i]];
    ##print STDERR "== $i == $#images == \n";
    ##print "<td>$image <td>$reimages{$image} <td> $annotator<td> $ruser<td>$i<tr>\n";
}
print "</table>";

if (!$q->param ) {
    PrintEntryForm();
} else {
    my $xml = "$xmlhead\n".$q->param('xml').
        "\n <!-- ANNOTATOR: ".lc($q->remote_user())." -->\n$xmlfoot\n";
    my $xmlfile = $q->param('image');
    $xmlfile =~ s/(.+)\..+/$1.xml/;
    open XM, ">$datadir/$xmlfile" || die "Error writing $xmlfile $!";
    $xml =~ s/<image.*?>/"<image uri='".$q->param('image')."'\/>"/e;
    print XM $xml;
    close XM;
    my $dir = getcwd;
    chdir $datadir;  
    my $xmlinvalid = `xmllint --valid --noout $xmlfile 2>&1`; 
    chdir $dir;
    if (!$xmlinvalid && $xml !~ /.*<visual>.*/s){
        $xmlinvalid = 'No &lt;visual&gt; element entered';
    }
    if ($xmlinvalid){
        $annotator = $q->param('annotator');
        $image = $q->param('image');
        PrintEntryForm($q->param('xml'));
        PrintErrorMessage("Error validating your picture description XML; please correct and resubmit:\n<br><pre>$xmlinvalid<pre>\n");
    }
    else {
        open AT, ">>$annotable" || die "Error reading $annotable $!";
        print AT lc($q->remote_user()).",".$q->param('image')."\n";
        ## print "--->".$q->remote_user().",".$q->param('image').",".$image."\n";        
        close AT;
        my $last = IsLastImage();
        #print "last=$last";
        PrintEntryForm()
            unless $last;
        PrintOKMessage("Annotation saved successfully.");
    }
}

sub PrintEntryForm {
    my $preventry = shift || '';
    
    print "<form action='annotate.cgi' method='POST'>\n";
    print "<table><tr><td>\n";
    print "<label for=\"xml\"><h3>Enter XML description of the visual shown</h3></label><br/>\n";
        print "You might like to consult <a href='https://www.oslomedicalcorpus.net/corpus-text-preparation-2/#visuals'>the annotation guidelines</a> and the <a href='https://luzs.gitlab.io/oslomedicalcorpus/DesnoyersTaxonomyHTML/Taxonomy.htm'>taxonomy table</a> for guidance.<br/>\n";
    print "<p><textarea rows=\"16\" cols=\"90\" name=\"xml\" placeholder=\"Enter XML tags for classification of visual\">$preventry</textarea></p>";
    print $q->submit(-value=>'Submit');
    print "\n</td><td>\n";
    print "<input type='hidden' id='image' name='image' value='$image'>\n";
    print "<input type='hidden' id='annotator' name='annotator' value='$annotator'>\n";
    print "<img src='$baseurl/$image' alt='$image'>\n";
    print "</td></tr></table>\n";
    print "</form>\n";
}

sub PrintOKMessage {
    my $msg = shift;
    print "<br><br><br><br><font color='green'>$msg</font>";
}

sub PrintErrorMessage {
    my $msg = shift;
    print "<br><br><br><br><font color='red'>$msg</font>";
}

sub PrintCloseHtml {
    print "</body></html>\n";
}


sub IsLastImage {
    my $j = $i+1;
    my $an = $annotators[$rand[$j]];
    my $im = $images[$rand[$j]];
    ##print "--> $annotator --> $image<br>";
    while  ( $an eq $ruser  || exists($reimages{$im} ) ){
        return(1)
            if ($j++ >= $#images);
        $an = $annotators[$rand[$j]];
        $im = $images[$rand[$j]];
        ##print "--> $annotator --> $image<br>";
        ##print "an --> $an im --> $im<br>";
    }
    return(0);
}
