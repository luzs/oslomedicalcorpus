
# Inter-annotator reliability study for OMC visuals

Relevant code and files:

- ira.R: R code for statistical assessment of inter-rater agreement

- ../util/summarisevisuals.pl: Extract visual descriptors from file.xml, flatten them and add the flattened version to a CSV file.

## Analysis:

From Henry's log table, get all annotation details:

https://airtable.com/shrHD9ceopmjheZjm

saved to `annotationdetails.csv`.

Import table to ira.R:

```r
source(ira.R)
## randomly assign second annotators to existing files
x <- getSecondAnnotatorTable(AD)
```

Randomly assigning by files, however, may not be the best policy, as
the number of visuals per file varies greatly and its distribution is
quite skewed:

```r
sortTableByColumn(x,'visuals')
   annotator2 ix annotator1  filename visuals
49       June  9   Kyunghye omc000010       0
15    Gustavo 10   Kyunghye omc000011       0
44       June 23    Hammood omc000024       0
1     Marcela 11    Gustavo omc000012       1
6     Marcela 19   Kyunghye omc000020       1
7     Marcela 21       Gina omc000022       1
41       Gina 39   Kyunghye omc000046       2
27   Kyunghye 42    Gustavo omc000053       2
17    Gustavo 48    Marcela omc000068       2
30   Kyunghye  6    Gustavo omc000007       3
2     Marcela 17    Hammood omc000018       3
48       June 18    Gustavo omc000019       3
4     Marcela 20   Kyunghye omc000021       4
24    Hammood 38    Gustavo omc000044       4
28   Kyunghye  7    Hammood omc000008       5
31   Kyunghye 49    Marcela omc000070       5
3     Marcela  8    Hammood omc000009       6
32   Kyunghye  2    Gustavo omc000002       7
46       June 40   Kyunghye omc000047       7
45       June 41    Marcela omc000048       8
43       June 31    Hammood omc000034       9
47       June 12   Kyunghye omc000013      12
22    Hammood 46    Marcela omc000063      12
8     Marcela 15    Hammood omc000016      14
21    Hammood 24    Gustavo omc000025      15
40       Gina 43       June omc000055      16
13    Gustavo 27    Marcela omc000029      20
14    Gustavo 45    Marcela omc000061      21
37       Gina  1    Marcela omc000001      30
25    Hammood 14    Gustavo omc000015      30
38       Gina 16    Marcela omc000017      33
12    Gustavo 34    Marcela omc000038      33
29   Kyunghye 13    Hammood omc000014      36
19    Hammood 44    Marcela omc000058      36
26   Kyunghye 33    Marcela omc000036      37
23    Hammood 22    Marcela omc000023      41
42       June  3    Hammood omc000003      44
39       Gina 47    Marcela omc000065      47
11    Gustavo 30    Hammood omc000033      49
18    Hammood  5    Gustavo omc000006      50
34       Gina 32    Gustavo omc000035      82
35       Gina 35    Marcela omc000039      83
16    Gustavo 26    Marcela omc000027     157
20    Hammood 36    Gustavo omc000041     165
10    Gustavo 37    Marcela omc000042     196
33   Kyunghye 25    Hammood omc000026     223
36       Gina 28    Marcela omc000031     333
5     Marcela  4   Kyunghye omc000004     430
9     Marcela 29    Gustavo omc000032     466
```

Visual counts can be found in `visualcount.csv`, generated as follows:

```sh
for f in `find /disk2/omc/text/ -name '*.xml'` ; do echo -n "$f," ; grep '<visual>'  $f | wc -l ; done
```

From this we can get estimates of the number of visuals annotated by
each annotator:

```r
tapply(x$visuals, x$annotator1, sum)
    Gina  Gustavo  Hammood     June Kyunghye  Marcela 
       1      828      389       16      456     1094 
```


## Generating summary of visuals


A detailed breakdown of the visuals annotated can be obtained by
running summarisevisuals.pl on all files XML files (on the Unix shell
command line):

```sh
perl ../util/summarisevisuals.pl /disk2/omc/text/*.xml > visuals.csv
```


## Generating samples for re-annotation

### Power Calculation

To get a rough estimate of the number of instances we would need to
annotate to get a reasonable inter-rater agreement (IRA) score, we assume
we are using Cohen's kappa score, and wish to reject the null hypothesis:

- H0: Cohen's kappa score for the OMC annotation for class, order and
  family is 0.4 or lower
  
and our alternative hypothesis is:

- Ha: The kappa score is at least 0.6

We use the [`irr` package](https://cran.r-project.org/package=irr),
and the `orderProbs()` etc functions defined in `ira.R`

```r
> N2.cohen.kappa(orderProbs(), k1=0.6, k0=0.4, power=.8)
[1] 76
> N2.cohen.kappa(classProbs(), k1=0.6, k0=0.4, power=.8)
[1] 87
> N2.cohen.kappa(familyProbs(), k1=0.6, k0=0.4, power=.8)
[1] 74
```

So, ignoring very low occurence orders and families, and assuming 2
annotators per visual, we would need to re-annotate around 87
visuals. We will sample a few more to be on the safe side. 

### Sampling

For this we will use ira.R function `iraSampleIndex()`. Let's sample
140 images, trying to balance annotators and classes, and setting a
minimum of 6 instances for each order and family (except where there
are less than 6 instances of each in the whole dataset):

```r
y <- iraSampleIndex(140, min=6)

summariseCategories(y)

ANNOTATOR:
    Gina  Gustavo  Hammood     June Kyunghye  Marcela 
       1       43       34        1       33       58 

CLASS:
analogram cosmogram  typogram 
       74        37        59 

ORDER:
cellulogram       curvigram  histocurvigram       histogram      morphogram      organigram puncticurvigram      punctigram 
         39               8              12              35               1               9               7              11 
    reigram     scriptogram        topogram 
         24              11              13 

FAMILY:
    absolute     cellulogramfamily       curvigramfamily               ecogram  histocurvigramfamily      organigramfamily 
          29                    39                     8                    13                    12                     9 
proportional puncticurvigramfamily      punctigramfamily                radial         reigramfamily     scriptogramfamily 
           6                     7                    11                     1                    24                    11 
 
write.csv(y, file='sampleira.csv')  
```

Note that because of the minimality requirements, the sampling
procedure generates more samples than asked for (in this case, 170,rather than 140),

## Web-based annotation system

A simple web-based annotation interface has been implemented in
`annotate.cgi`. The program is login-protected, and it ensures that no
annotator will be offered a visual for annotation which they have
annotated before.

The program can be accessed by opening

<https://genealogies.mvm.ed.ac.uk/omc/ira/>

It presents each sample visual specified in `sampleria.csv`, on at a
time. The user should enter a complete XML specification of the
visual, as per
[taxonomy](https://luzs.gitlab.io/oslomedicalcorpus/DesnoyersTaxonomyHTML/Taxonomy.htm). 
For example, 

```xml
<visual>
  <image uri=""/>
  <caption></caption>
  <class>
    <cosmogram>
      <topogram>
        <ecogram>
          <descriptive>
            <description>Map of Stowe</description>
          </descriptive>
        </ecogram>
      </topogram>
    </cosmogram>
  </class>
</visual> 
```

The program checks the validity of the specification against the OMC
DTD. If the user enters an invalid specification an error message will
be shown and the user will be asked to correct and resubmit it.

## Computing inter-annotator agreement statistics

### Obtaining and preparing the data

The table containing images and annotators was downloaded into
`./data/` from the genealogies website, along with the annotations
saved in XML format by annotation.cgi:

```sh
rsync -av goked:/var/www/html/omc/ira/annotated.csv  data
rsync -av goked:/var/www/html/omc/ira/data/*.xml  data
```

Now we need to extract the annotations from the XML files and put them
into tabular (CSV) format for processing by the statistics software
(R). We use `summarisevisuals.pl` for this:

```sh
perl ../../util/summarisevisuals.pl -c *.xml > visuals-2nd.csv
```

The resulting file contains the new (annotator2) annotations. Now, in
R, having loaded `ira.R`, we have access to two datasets: `VC1` and
`VC2`, containing the annotations by the original annotators and the
new annotations entered throug the web-based system. We can now
concatenate these tables into a single annotation matrix:

```r
classes <- getAnnotationsTable(VD2, variable='class', vd1=VD1)

classes
                                image annotator1 annotator2
1             omc000001-figure-14.png  analogram  analogram
2             omc000001-figure-19.png  analogram  analogram
3             omc000001-figure-26.png   typogram   typogram
4             omc000001-figure-29.png  analogram  analogram
5              omc000001-figure-6.png  analogram   typogram
6                omc000003-fig-04.png  analogram  analogram
7                omc000003-fig-15.png  analogram  analogram
[...]
```

We can do the same for other levels of the visuals taxonomy: `family`,
`order`, etc. E.g.:

```r
families <- getAnnotationsTable(VD2, variable='family', vd1=VD1)
```

The first arguments of `getAnnotationsTable()` is an ellipsis, so we
can add as many annotators as we want. If we had a VD3, we could add
it simply by doing: 

```r
families <- getAnnotationsTable(VD2, VD3, variable='family', vd1=VD1)

families
                                image            annotator1            annotator2            annotator3
1             omc000001-figure-14.png      punctigramfamily              absolute              absolute
2             omc000001-figure-19.png              absolute              absolute              absolute
3             omc000001-figure-26.png      organigramfamily      organigramfamily      organigramfamily
4             omc000001-figure-29.png      punctigramfamily              absolute              absolute
```

### Raw agreement statistics:

#### Class

Class shows very high raw agreement, with nearly 95% agreement on the
169 visuals re-annotated:

```r
classes <- getAnnotationsTable(VD2, variable='class', vd1=VD1)
prop.table(table(as.character(classes$annotator2)==as.character(classes$annotator1)))

     FALSE       TRUE 
0.05325444 0.94674556 
```

#### Order and Family

The annotators agree 81.7% of the time as regards visual order and family:

```r
prop.table(table(as.character(families$annotator2)==as.character(families$annotator1)))
   FALSE     TRUE 
0.183432 0.816568 
```

### Kappa statistics

To establish the extent to which these levels of agreement occurred
despite chance, we employ Cohen's $\kappa$ statistic, which is defined
as follows:

```math
\kappa = \frac{A_o - A_e}{1 - A_e}
```

where $`A_o`$ is the observed agreement (as a percentage of the total
number of annotated visuals) and $`A_e`$ is the number of agreements one
would expect to occur by chance. $`A_e`$ is computed as follows:

```math
A_e = \frac{1}{N^2} \sum_k n_{k1}n_{k2}
```

where $`N`$ is the number of annotated visuals, $`k`$ is the number of
categories,  and $`n_{ki}`$ is the number of times rater $`i`$ predicted
category $`k`$. Thus, $`\kappa = 1`$ indicates complete agreement, and
$`\kappa = 0`$ indicates agreement by chance. 

The computed statistics (using R's `irr` package) are:

```r
kappa2(classes[,c(2,3)], 'unweighted')
 Cohen's Kappa for 2 Raters (Weights: unweighted)

 Subjects = 169 
   Raters = 2 
    Kappa = 0.917 

        z = 16.4 
  p-value = 0 

kappa2(families[,c(2,3)], 'unweighted')
 Cohen's Kappa for 2 Raters (Weights: unweighted)

 Subjects = 169 
   Raters = 2 
    Kappa = 0.789 

        z = 28 
  p-value = 0 

kappa2(order[,c(2,3)], 'unweighted')
 Cohen's Kappa for 2 Raters (Weights: unweighted)

 Subjects = 169 
   Raters = 2 
    Kappa = 0.785 

        z = 26.2 
  p-value = 0 

```

In all cases we have been able to successfully reject H0, and
therefore accept HA ($`\kappa > 0.6`$). 

## Qualitative analysis of inter-annotator disagreement

The [results/](results/) folder contains detailed lists of the disagreements
recorded in the experiment, at each level of the hierarchy, in
Markdown format. These lists were generated using
`ira:writeMismatchMD()`. Running, for example:

```r
writeMismatchMD(variable='family')
```

generates a list of all mismatches at the level of 'family' in the
visuals taxonomy, excluding the ones that also involve a mismatch
above the `variable` in the hierarchy (in this case, excluding all
mismatches at the 'class' level, which is the level imeediately above
'family'). There were 9 'class' mismatches, an additional 22 'family'
mismatches, and no additional 'order' mismatches for this re-annotated
sample of 169 visuals. 

The links below display the visuals and annotation details for:

- [Visual 'class' disagreements](results/class-mismatch.md)  and

- [Visual 'family' disagreements](results/family-mismatch.md)
